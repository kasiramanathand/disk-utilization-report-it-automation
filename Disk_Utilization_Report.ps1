$Servers=Get-Content -Path C:\ServerNames.txt
$Username="Enter Username"
$Password=ConvertTo-SecureString -String "Enter Password" -AsPlainText -Force
$Credential = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $Username,$Password
$Date = Get-Date -Format dd-MM-yyyy
 
foreach($Server in $Servers)
{
Get-WmiObject Win32_LogicalDisk -ComputerName $Server -Credential $Credential -filter "DriveType = '3'" | Select PSComputername,VolumeName,DeviceID,
@{Name="Size";Expression={ "{0:N1}" -f ($_.Size/1GB)}},
@{Name="UsedSpace";Expression={ "{0:N1}" -f (($_.Size/1GB) - ($_.FreeSpace/1GB))}},
@{Name="FreeSpace";Expression={ "{0:N1}" -f ($_.FreeSpace/1GB)}},
@{Name="Percent";Expression={ "{0:P0}" -f ($_.FreeSpace/$_.Size)}} | Export-Csv -Path C:\StorageReport_$Date.CSV -NoTypeInformation
}
